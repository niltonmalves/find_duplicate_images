from collections import defaultdict
from glob import glob
from os.path import basename
import hashlib
import sys, os
import csv
from multiprocessing import Pool
from tqdm import tqdm

lista_todas_rede = glob('C:\\Users\\alves\\Documents\\Gitlab\\find_duplicated\\teste\\*')

def get_keys(args):
    file = args
    with open (file,'rb') as f:
        key=hashlib.md5(f.read()).hexdigest()
    return (key,file)

if __name__ == '__main__': 

    with Pool(processes=8) as p:
        lista_key = list(tqdm(p.map(get_keys,lista_todas_rede),total=len(lista_todas_rede)))

    print(lista_key)
    lista_key =sorted(lista_key, key=lambda hashnum: hashnum[0])
    infos = defaultdict(list)
    for aux in lista_key:
        key,file = aux
        infos[key].append(file)
    for key in infos:
        #print(key)
        size = len(infos[key])
        if size>1:
            store = key
            for i in range(size-1):
                print(infos[key][i])
                #os.remove(infos[key][i])

